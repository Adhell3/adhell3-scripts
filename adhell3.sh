#!/bin/sh
clear
param1=$1
param2=$2
jdk8_folder=jdk8u202-b08
jdk8_Mac_file=OpenJDK8U-jdk_x64_mac_hotspot_8u202b08.tar.gz
jdk8_wildcard_file=OpenJDK8U-jdk_x64_*.tar.gz
jdk8_Mac_url=https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/$jdk8_folder/$jdk8_Mac_file

jdk8_Linux_file=OpenJDK8U-jdk_x64_linux_hotspot_8u202b08.tar.gz
jdk8_Linux_url=https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u202-b08/$jdk8_Linux_file

android_sdk_folder=android-sdk
android_sdk_Mac_file=sdk-tools-darwin-4333796.zip
android_sdk_wildcard_file=sdk-tools-*.zip
android_sdk_Mac_url=https://dl.google.com/android/repository/$android_sdk_Mac_file

android_sdk_Linux_file=sdk-tools-linux-4333796.zip
android_sdk_Linux_url=https://dl.google.com/android/repository/$android_sdk_Linux_file
tools_folder=tools

adhell3_file=adhell3-master.zip
adhell3_url=https://gitlab.com/fusionjack/adhell3/-/archive/master/$adhell3_file
adhell3_folder=adhell3-master
app_folder=app
libs_folder=app/libs

knoxsdk_jar_file=knoxsdk.jar
supportlib_jar_file=supportlib.jar
app_properties_file=app.properties

if [ "$1" == "setup" ] || ([ "$1" == "clean" ] && [ "$2" == "setup" ]); then
   if [ "$1" == "clean" ]; then
      echo "Deleting $jdk8_folder ..."
      rm -rf $jdk8_folder

      echo "Deleting $android_sdk_folder ..."
      rm -rf $android_sdk_folder

      echo "Deleting $tools_folder ..."
      rm -rf $tools_folder

      echo "Deleting $jdk8_wildcard_file ..."
      rm $jdk8_wildcard_file

      echo "Deleting $android_sdk_wildcard_file ..."
      rm $android_sdk_wildcard_file
   fi
   echo
   if [ "$(uname)" == "Darwin" ]; then
      echo "Detected Mac OS platform"
      platform=Mac
   elif [ "$(uname)" == "Linux" ]; then
      echo "Detected Linux platform"
	  platform=Linux
   else 
      echo "Detected unkown platform ($(uname)), force using Linux platform"
      platform=Linux
   fi
 if [ "$platform" == "Mac" ]; then
   if [ ! -f $jdk8_wildcard_file ]; then
      echo "Downloading $jdk8_Mac_file ..."
      curl -LO $jdk8_Mac_url
   else
      echo "Found $jdk8_Mac_file"
   fi

   if [ ! -d $jdk8_folder ]; then
      echo "Extracting $jdk8_Mac_file ..."
      tar xvf $jdk8_Mac_file
   fi

   if [ ! -f $android_sdk_wildcard_file ]; then
      echo "Downloading $android_sdk_Mac_file ..."
      curl -O $android_sdk_Mac_url
   else
      echo "Found $android_sdk_Mac_file"
   fi

   if [ ! -d $tools_folder ]; then
      echo "Extracting $android_sdk_Mac_file ..."
      unzip $android_sdk_Mac_file
   fi

   if [ ! -d $android_sdk_folder ]; then
      echo "Configuring Android SDK ..."
      mkdir $android_sdk_folder
      export JAVA_HOME=$PWD/$jdk8_folder/Contents/Home/
      echo "y" | ./$tools_folder/bin/sdkmanager "platform-tools" --sdk_root="$PWD/$android_sdk_folder"
   fi
fi

 if [ "$platform" == "Linux" ]; then
   if [ ! -f $jdk8_wildcard_file ]; then
      echo "Downloading $jdk8_Linux_file ..."
      curl -LO $jdk8_Linux_url
   else
      echo "Found $jdk8_Linux_file"
   fi

   if [ ! -d $jdk8_folder ]; then
      echo "Extracting $jdk8_Linux_file ..."
      tar xvf $jdk8_Linux_file
   fi

   if [ ! -f $android_sdk_wildcard_file ]; then
      echo "Downloading $android_sdk_Linux_file ..."
      curl -O $android_sdk_Linux_url
   else
      echo "Found $android_sdk_Linux_file"
   fi

   if [ ! -d $tools_folder ]; then
      echo "Extracting $android_sdk_Linux_file ..."
      unzip $android_sdk_Linux_file
   fi

   if [ ! -d $android_sdk_folder ]; then
      echo "Configuring Android SDK ..."
      mkdir $android_sdk_folder
      export JAVA_HOME=$PWD/$jdk8_folder
      echo "y" | ./$tools_folder/bin/sdkmanager "platform-tools" --sdk_root="$PWD/$android_sdk_folder"
   fi
fi

elif [ "$1" == "build" ] || ([ "$1" == "clean" ] && [ "$2" == "build" ]); then
   if [ "$1" == "clean" ]; then     
   
   if [ "$(uname)" == "Darwin" ]; then
      echo "Detected Mac OS platform"
      platform=Mac
   elif [ "$(uname)" == "Linux" ]; then
      echo "Detected Linux platform"
	  platform=Linux
   else 
      echo "Detected unkown ($(uname)) platform, force using Linux platform"
      platform=Linux
   fi 
     
  	 if [ -f $adhell3_file ]; then
         echo "Deleting $adhell3_file ..."
         rm $adhell3_file
      fi

      if [ -d $adhell3_folder ]; then
         echo "Deleting $adhell3_folder folder ..."
         rm -rf $adhell3_folder
      fi

      echo "Getting latest adhell3 source code from gitlab ..."
      curl -O $adhell3_url
      echo "Extracting $adhell3_file ..."
      unzip $adhell3_file
   fi

   if [ ! -d $jdk8_folder ]; then
      echo "Missing $jdk8_folder folder, please run 'bash adhell3.sh setup'"
      exit 1
   fi

   if [ ! -d $android_sdk_folder ]; then
      echo "Missing $android_sdk_folder folder, please run 'bash adhell3.sh setup'"
      exit 1
   fi

   if [ ! -f $knoxsdk_jar_file ]; then
      echo "Missing $knoxsdk_jar_file file, please get it from Samsung SEAP and put it in the same folder where this script is located"
      exit 1
   fi

   if [ ! -f $supportlib_jar_file ]; then
      echo "Missing $supportlib_jar_file file, please get it from Samsung SEAP and put it in the same folder where this script is located"
      exit 1
   fi

   if [ ! -f $app_properties_file ]; then
      echo "Missing $app_properties_file file, please create it, set your application id and put it in the same folder where this script is located"
      exit 1
   fi
   
   if [ "$platform" == "Mac" ]; then
   export JAVA_HOME=$PWD/$jdk8_folder/Contents/Home/
   fi
   if [ "$platform" == "Linux" ]; then
   export JAVA_HOME=$PWD/$jdk8_folder
   fi
   export ANDROID_HOME=$PWD/$android_sdk_folder
   
   if ([ "$2" == "install" ] || [ "$3" == "install" ]) ; then
   	echo "Building and installing apk ..."
   else
   	echo "Building apk ..."
   fi
   cd $adhell3_folder
   cp ../$app_properties_file $app_folder/$app_properties_file
   
   if [ ! -d $libs_folder ]; then
      mkdir $libs_folder
   fi

   cp ../$knoxsdk_jar_file $libs_folder/$knoxsdk_jar_file
   cp ../$supportlib_jar_file $libs_folder/$supportlib_jar_file

   chmod +x gradlew
   if ([ "$2" == "install" ] || [ "$3" == "install" ]) ; then
   ./gradlew clean installDebug --no-daemon
   else
   ./gradlew clean assembleDebug --no-daemon
    cp -f $app_folder/build/outputs/apk/debug/app-debug.apk  ../adhell3_`cat $app_folder/build.properties | grep "number=" | cut -d= -f2`.apk
   fi
   echo
   cat $app_folder/build.properties 
      
else
   echo "Unknown parameter"
fi
